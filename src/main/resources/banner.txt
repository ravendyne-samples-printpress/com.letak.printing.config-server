${Ansi.GREEN} :: Application : ${spring.application.name} ::
${Ansi.BRIGHT_GREEN}     __      _        _           ___      _       _   _             
${Ansi.BRIGHT_GREEN}    / /  ___| |_ __ _| | __      / _ \_ __(_)_ __ | |_(_)_ __   __ _ 
${Ansi.BRIGHT_GREEN}   / /  / _ \ __/ _` | |/ /____ / /_)/ '__| | '_ \| __| | '_ \ / _` |
${Ansi.BRIGHT_GREEN}  / /__|  __/ || (_| |   <_____/ ___/| |  | | | | | |_| | | | | (_| |
${Ansi.BRIGHT_GREEN}  \____/\___|\__\__,_|_|\_\    \/    |_|  |_|_| |_|\__|_|_| |_|\__, |
${Ansi.BRIGHT_GREEN}                                                               |___/ 
${Ansi.GREEN} :: Spring Boot${spring-boot.formatted-version} ::
${Ansi.GREEN} :: Application Version : ${application.version} ::
${Ansi.GREEN} :: Application Title : ${application.title} ::
${Ansi.DEFAULT}